//
//  AddRecipeViewController.h
//  RecipeWithDB
//
//  Created by Adam Farrell on 6/22/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ViewController.h"
#import "UIColor+CustomColors.h"
#import "UIFont+CustomFonts.h"
#import <Parse/Parse.h>
#import "CustomButton.h"

@interface AddRecipeViewController : UIViewController
{
    UIImageView* recipeImage;
    UITextField* recipeTitle;
    UITextView* recipeInstructions;
    UIButton* submitButton;
    PFFile* imageToUpload;
}
@end
