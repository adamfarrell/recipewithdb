//
//  AddRecipeViewController.m
//  RecipeWithDB
//
//  Created by Adam Farrell on 6/22/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "AddRecipeViewController.h"

@interface AddRecipeViewController ()

@end

@implementation AddRecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor primaryBackgroundColor];
    
    recipeImage = [UIImageView new];
    recipeImage.translatesAutoresizingMaskIntoConstraints = NO;
    recipeImage.contentMode = UIViewContentModeScaleAspectFit;
    recipeImage.image = [UIImage imageNamed:@"placeholder"];
    [self.view addSubview:recipeImage];
    
    recipeTitle = [UITextField new];
    recipeTitle.translatesAutoresizingMaskIntoConstraints = NO;
    recipeTitle.inputAccessoryView = [self editingView];
    recipeTitle.font = [UIFont primaryTitleFont];
    recipeTitle.textColor = [UIColor primaryFontColor];
    recipeTitle.backgroundColor = [UIColor secondaryBackgroundColor];
    recipeTitle.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"Recipe Title"];
    [self.view addSubview:recipeTitle];
    
    recipeInstructions = [UITextView new];
    recipeInstructions.translatesAutoresizingMaskIntoConstraints = NO;
    recipeInstructions.inputAccessoryView = [self editingView];
    recipeInstructions.font = [UIFont primaryFont];
    recipeInstructions.backgroundColor = [UIColor secondaryBackgroundColor];
    recipeInstructions.textColor = [UIColor primaryFontColor];
    [self.view addSubview:recipeInstructions];
    
    submitButton = [CustomButton new];
    submitButton.translatesAutoresizingMaskIntoConstraints = NO;
    [submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(submitTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submitButton];
    
    
    NSDictionary* views = NSDictionaryOfVariableBindings(recipeImage, recipeInstructions, recipeTitle, submitButton);
    NSDictionary* metrics = @{@"imageSide":@125.0,@"padding":@15.0,@"fromTop":@50.0,@"height":@300.0};
    
    //set up constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|->=padding-[recipeImage(imageSide)]->=padding-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[recipeTitle]-padding-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[recipeInstructions]-padding-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|->=padding-[submitButton(300.0)]->=padding-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-fromTop-[recipeImage(imageSide)]-padding-[recipeTitle]-padding-[recipeInstructions]-padding-[submitButton]-padding-|" options:NSLayoutFormatAlignAllCenterX metrics:metrics views:views]];
}

-(UIView*)editingView {
    UIView* editView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
    editView.backgroundColor = [UIColor primaryBackgroundColor];
    
    CustomButton* doneButton = [[CustomButton alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneTouched:) forControlEvents:UIControlEventTouchUpInside];
    [editView addSubview:doneButton];
    return editView;
}

-(void)doneTouched:(id)sender {
    [self.view endEditing:YES];
}

-(void)submitTouched:(id)sender {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://api.parse.com/1/classes/Recipe"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"So2Ccw81sHJgU3I8laOC3vyg6Jsb0uueXnN7Rpc0"forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:@"Snztv10oX9ewR7zg2IhmHRG8G0CJKU9zaI64Y4OF" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    NSDictionary* diction = @{@"RecipeTitle":recipeTitle.text, @"RecipeInstructions":recipeInstructions.text};
    
    NSError* error;
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:diction
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
    [request setHTTPBody:dataFromDict];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    NSString *requestReply = [[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding];
        NSLog(@"requestReply: %@", requestReply);
    
    
    
    //I tried LONG and HARD to get REST HTTP to load what the Images File. I could not get it to do it. So I did it with parses api. I will have this bit commented out. Put it in just so I knew I could get images onto parse as I will need it for my final project.
//    [self prepareImageForUpload];
//    PFObject* recipe = [PFObject objectWithClassName:@"Recipe"];
//    recipe[@"RecipeTitle"] = recipeTitle.text;
//    recipe[@"RecipeInstructions"] = recipeInstructions.text;
//    recipe[@"RecipeImage"] = imageToUpload;
//    [recipe saveInBackgroundWithBlock:^(BOOL succeeded, NSError* error){
//        if (succeeded) {
//            //success
//        } else {
//            //failed
//        }
//    }];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)prepareImageForUpload {
    NSData* photoData = UIImagePNGRepresentation(recipeImage.image);
    imageToUpload = [PFFile fileWithData:photoData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
