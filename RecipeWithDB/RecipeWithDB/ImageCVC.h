//
//  ImageCVC.h
//  autolayout
//
//  Created by Adam Farrell on 6/16/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCVC : UICollectionViewCell
@property (nonatomic, retain) UIImageView* image;
@end
