//
//  ImageCVC.m
//  autolayout
//
//  Created by Adam Farrell on 6/16/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ImageCVC.h"

@implementation ImageCVC

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self.image setContentMode:UIViewContentModeScaleAspectFit];
        [self.image setImage:[UIImage imageNamed:NULL]];
        [self addSubview:self.image];
    }
    return self;
}
//
//-(void)prepareForReuse {
//    [super prepareForReuse];
//}

@end
