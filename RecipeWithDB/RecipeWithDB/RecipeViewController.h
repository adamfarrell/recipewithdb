//
//  RecipeViewController.h
//  autolayout
//
//  Created by Adam Farrell on 6/16/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ViewController.h"
#import "UIColor+CustomColors.h"
#import "UIFont+CustomFonts.h"
#import <Parse/Parse.h>

@interface RecipeViewController : UIViewController
@property (nonatomic, strong)PFObject* recipeDictionary;
@end
