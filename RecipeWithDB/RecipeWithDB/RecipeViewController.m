//
//  RecipeViewController.m
//  autolayout
//
//  Created by Adam Farrell on 6/16/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "RecipeViewController.h"

@interface RecipeViewController ()

@end

@implementation RecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor primaryBackgroundColor];

    
    UIImageView* recipeImage = [UIImageView new];
    recipeImage.translatesAutoresizingMaskIntoConstraints = NO;
    recipeImage.contentMode = UIViewContentModeScaleAspectFit;
//    recipeImage.image = [UIImage imageNamed:[self.recipeDictionary objectForKey:@"RecipeImage"]];
    
    PFFile* thisImage = [self.recipeDictionary objectForKey:@"RecipeImage"];
    [thisImage getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
        if (!error) {
            recipeImage.image = [UIImage imageWithData:imageData];
        }
    }];
    [self.view addSubview:recipeImage];
    
    UILabel* recipeTitle = [UILabel new];
    recipeTitle.translatesAutoresizingMaskIntoConstraints = NO;
    recipeTitle.font = [UIFont primaryTitleFont];
    recipeTitle.text = [self.recipeDictionary objectForKey:@"RecipeTitle"];
    recipeTitle.textColor = [UIColor primaryFontColor];
    recipeTitle.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:recipeTitle];
    
    UITextView* recipeInstructions = [UITextView new];
    recipeInstructions.translatesAutoresizingMaskIntoConstraints = NO;
    recipeInstructions.font = [UIFont primaryFont];
    recipeInstructions.backgroundColor = [UIColor primaryBackgroundColor];
    recipeInstructions.textColor = [UIColor primaryFontColor];
    recipeInstructions.editable = NO;
    [recipeInstructions setText:[[self.recipeDictionary objectForKey:@"RecipeInstructions"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"]];
    [self.view addSubview:recipeInstructions];
    
    NSDictionary* views = NSDictionaryOfVariableBindings(recipeImage, recipeInstructions, recipeTitle);
    NSDictionary* metrics = @{@"imageSide":@125.0,@"padding":@15.0,@"fromTop":@50.0,@"height":@300.0};
    
    //set up constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|->=padding-[recipeImage(imageSide)]->=padding-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[recipeTitle]-padding-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[recipeInstructions]-padding-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-fromTop-[recipeImage(imageSide)]-padding-[recipeTitle]-padding-[recipeInstructions]|" options:NSLayoutFormatAlignAllCenterX metrics:metrics views:views]];
    
    
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
