//
//  ViewController.h
//  RecipeWithDB
//
//  Created by Adam Farrell on 6/22/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "UIColor+CustomColors.h"
#import "ImageCVC.h"
#import "RecipeViewController.h"
#import "AddRecipeViewController.h"

@interface ViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    NSArray* arrayOfRecipes;
    UICollectionView* recipeList;
}


@end

