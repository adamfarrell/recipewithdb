//
//  ViewController.m
//  RecipeWithDB
//
//  Created by Adam Farrell on 6/22/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
//    testObject[@"foo"] = @"bar";
//    [testObject saveInBackground];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTouched:)];
    self.view.backgroundColor = [UIColor primaryBackgroundColor];
    
    
    
    UICollectionViewFlowLayout* recipeFlow = [[UICollectionViewFlowLayout alloc]init];
    
    recipeList = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) collectionViewLayout:recipeFlow];
    recipeList.delegate = self;
    recipeList.dataSource = self;
    recipeList.backgroundColor = [UIColor primaryBackgroundColor];
    
    [recipeList registerClass:[ImageCVC class] forCellWithReuseIdentifier:@"cell"];
    
    [self.view addSubview:recipeList];
    
}

-(ImageCVC *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCVC* cell = (ImageCVC*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    PFObject* temp = arrayOfRecipes[indexPath.row];
    NSLog(@"%@",temp);
    PFFile* thisImage = [temp objectForKey:@"RecipeImage"];
    [thisImage getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
        if (!error) {
            cell.image.image = [UIImage imageWithData:imageData];
        } else {
            //failed
        }
    }];
    if (cell.image.image == NULL) {
        cell.image.image = [UIImage imageNamed:@"placeholder"];
    }
//    cell.image.image = [UIImage imageNamed:[temp objectForKey:@"RecipeImage"]];
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSLog(@"%lu",(unsigned long)arrayOfRecipes.count);
    return arrayOfRecipes.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width / 2 - 20, 200);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(12, 12, 12, 12);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    PFObject* selectedRecipe = arrayOfRecipes[indexPath.row];
    RecipeViewController* recipe = [RecipeViewController new];
    recipe.recipeDictionary = selectedRecipe;
    [self.navigationController pushViewController:recipe animated:YES];
}

-(void)addTouched:(id)sender {
    AddRecipeViewController* addRecipeVC = [[AddRecipeViewController alloc]init];
    [self.navigationController pushViewController:addRecipeVC animated:YES];
}

-(void)loadData {
    //    Parse Retrieve Object
    UIView* loading = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    loading.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:loading];
    
    UILabel* loadingLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 50, self.view.bounds.size.width - 40, 50)];
    loadingLabel.font = [UIFont primaryTitleFont];
    loadingLabel.textColor = [UIColor whiteColor];
    loadingLabel.text = @"Loading...";
    loadingLabel.textAlignment = NSTextAlignmentCenter;
    [loading addSubview:loadingLabel];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Recipe"];
    [query findObjectsInBackgroundWithBlock:^(NSArray* recipes, NSError *error) {
        if (!error) {
            // The find succeeded.
            arrayOfRecipes = [NSArray arrayWithArray:recipes];
            //            for (PFObject* obj in arrayOfRecipes) {
            //                NSLog(@"%@", obj);
            //            }
            [recipeList reloadData];
            [loading removeFromSuperview];
            //NSLog(@"%lu",(unsigned long)arrayOfRecipes.count);
        } else {
            // Log details of the failure
            //NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
